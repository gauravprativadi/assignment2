#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup() {
	orbCount = 10;
	totalOrbs = orbCount * orbCount * orbCount;
	for (int x = 0; x < orbCount; x++) {
		for (int y = 0; y < orbCount; y++) {
			for (int z = 0; z < orbCount; z++) {
				int currOrb = (((x * orbCount) + y) * orbCount) + z;
				
				ofSpherePrimitive tempOrb;
				tempOrb.setPosition(x * 20, y * 20, z * 20);
				tempOrb.setRadius(10);
				orbs1.push_back(tempOrb);
				
			}
		}
	}


	light1.setDiffuseColor(ofColor(255, 250, 130));
	light1.setPosition(-100, 100, 500);
	light1.enable();

	light2.setDiffuseColor(ofColor(155, 150, 230));
	light2.setPosition(-400, -200, 0);
	light2.enable();
}

//--------------------------------------------------------------
void ofApp::update() {
	noiseTime += 0.0005;
	//noiseTime = (float)ofGetElapsedTimeMillis();
	for (int x = 0; x < orbCount; x++) {
		for (int y = 0; y < orbCount; y++) {
			for (int z = 0; z < orbCount; z++) {
				int currOrb = (((x * orbCount) + y) * orbCount) + z;
				float myNoise = 2 * ofSignedNoise(x / 5.0,
					y / 5.0 + noiseTime * 10,
					z / 5.0,
					noiseTime);
				float myNoiseX = 600 * ofNoise(x + noiseTime * 10, 0, z);
				float myNoiseY = 400 * ofNoise(x, y + noiseTime * 20, 0);
				float myNoiseZ = 500 * ofNoise(0, y, z + noiseTime * 30);

				orbs1[currOrb].setPosition(myNoiseX, myNoiseY, myNoiseZ);
				orbs1[currOrb].setScale(myNoise);

			}
		}
	}
}

//--------------------------------------------------------------
void ofApp::draw() {
	
	ofEnableDepthTest();
	cam.begin();

	ofTranslate(-300, 100, -300);
	ofRotateDeg(45, ofNoise(noiseTime, 0, 0), ofSignedNoise(0, noiseTime, 0), 0);
	for (int i = 0; i < totalOrbs; i++) {
		ofColor myColor;
		myColor.r = ofRandom(0, 255);
		myColor.g = ofRandom(0, 255);
		myColor.b = ofRandom(0, 255);
		ofSetColor(myColor);
		orbs1[i].draw();
	}
	cam.end();
	ofDisableDepthTest();
	cam.draw();
}
