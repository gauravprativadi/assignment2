#pragma once

#include "ofMain.h"

class ofApp : public ofBaseApp {

public:
	void setup();
	void update();
	void draw();
	vector <ofSpherePrimitive> orbs1;
	ofEasyCam cam;
	ofLight light1, light2;
	float noiseTime;
	int orbCount;
	int totalOrbs;
};
